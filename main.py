from fastapi import FastAPI

app = FastAPI()

@app.get("/")
def read_index():
    return {"location": "index"}

@app.get("/secure")
def trigger_scraping():
    return {"location": "secure"}
